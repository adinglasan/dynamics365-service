FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/dynamics365-service

WORKDIR /home/node/dynamics365-service

# Install dependencies
# RUN npm install pm2@2.6.1 -g

# CMD pm2-docker --json app.yml
CMD ["node", "app.js"]
