'use strict'

let reekoh = require('reekoh')
let plugin = new reekoh.plugins.Service()
/* eslint-disable new-cap */
const rkhLogger = new reekoh.logger('dynamics365-service')

const get = require('lodash.get')

plugin.on('data', (data) => {
  const request = require('request')
  let oDataKey = plugin.config.odataMethodKey || 'odata'
  let method = get(data, oDataKey)

  let headers = {
    'OData-MaxVersion': '4.0',
    'OData-Version': '4.0',
    'Cache-Control': 'no-cache',
    'Authorization': 'Bearer ' + data['access_token'],
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }

  var url = plugin.config.tenant + '/api/data/v8.1/' + plugin.config.entitySchema
  if (method !== null && method !== '' && method !== undefined) {
    url += '?' + method
  }

  var options = {
    url: url,
    method: 'GET',
    headers: headers,
    json: true
  }

  request(options, function (error, response, body) {
    if (!error) {
      plugin.pipe(data, body)
      .then(() => {
        plugin.log({
          title: 'Microsoft Dynamics 365 Service',
          result: body
        })
      })
      .catch(plugin.logException)
    } else {
      plugin.logException(error)
    }
  })
})

plugin.once('ready', () => {
  plugin.log('Dynamics 365 Service has been initialized.')
  rkhLogger.info('Dynamics 365 Service has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
